<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'id' => 1,
            'name' => 'Thomas A. Anderson',
            'username' => 'neo',
            'password' => 'abc123',
        ]);

        factory(App\Models\User::class, 19)->create();
    }
}
