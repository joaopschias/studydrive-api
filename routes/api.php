<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'LoginAPIController@register');
Route::post('login', 'LoginAPIController@login');
Route::get('users/top-week-favoriters', 'UserAPIController@topWeekFavoriters');
Route::get('photos/recent-favorites', 'PhotoAPIController@recentFavorites');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('me', 'UserAPIController@me');

    Route::patch('users/{id}', 'UserAPIController@update')->where('id', '[0-9]+');
    Route::get('users/favorite-photos', 'UserAPIController@favoritePhotos');
    Route::post('users/favorite-photo', 'UserAPIController@favoritePhoto');
    Route::delete('users/unfavorite-photo/{photo_id}', 'UserAPIController@unfavoritePhoto');
    Route::resource('users', 'UserAPIController');

    Route::resource('photos', 'PhotoAPIController');
});
