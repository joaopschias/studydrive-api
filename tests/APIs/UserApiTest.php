<?php namespace Tests\APIs;

use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Tests\Traits\MakeUserTrait;
use Tests\ApiTestTrait;

class UserApiTest extends TestCase
{
    use MakeUserTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @var string $token
     */
    protected $token;

    /**
     * @var User
     */
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = Auth::loginUsingId(1);
        $this->token = $this->user->createToken('Bearer ', [])->accessToken;
    }

    /**
     * @test
     */
    public function testIndexUser()
    {
        $response = $this->json('GET', '/api/users', [], [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testCreateUser()
    {
        $user = $this->fakeUserData(['password_confirmation' => 'abc123']);
        $this->response = $this->json('POST', '/api/users', $user, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);
        $this->assertApiResponse($user);
    }

    /**
     * @test
     */
    public function testReadUser()
    {
        $user = $this->withoutMiddleware()->makeUser();
        $this->response = $this->json('GET', '/api/users/'.$user->id, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);
        $this->assertApiResponse($user->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUser()
    {
        $user = $this->withoutMiddleware()->makeUser();
        $editedUser = $this->fakeUserData(['password_confirmation' => 'abc123']);

        $this->response = $this->json('PATCH', '/api/users/'.$user->id, $editedUser, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);
        $this->assertApiResponse($editedUser);
    }

    /**
     * @test
     */
    public function testDeleteUser()
    {
        $user = $this->withoutMiddleware()->makeUser();
        $this->response = $this->json('DELETE', '/api/users/'.$user->id, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);
        $this->assertApiSuccess();

        $this->response = $this->json('GET', '/api/users/'.$user->id, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);
        $this->response->assertStatus(404);
    }
}
