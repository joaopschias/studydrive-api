<?php namespace Tests\APIs;

use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Tests\Traits\MakePhotoTrait;
use Tests\ApiTestTrait;

class PhotoApiTest extends TestCase
{
    use MakePhotoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @var string $token
     */
    protected $token;

    /**
     * @var User
     */
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = Auth::loginUsingId(1);
        $this->token = $this->user->createToken('Bearer ', [])->accessToken;
    }

    /**
     * @test
     */
    public function testIndexPhoto()
    {
        $response = $this->json('GET', '/api/photos', [], [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testCreatePhoto()
    {
        $photo = $this->fakePhotoData();
        $this->response = $this->json('POST', '/api/photos', $photo, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);

        $this->assertApiResponse($photo);
    }

    /**
     * @test
     */
    public function testReadPhoto()
    {
        $photo = $this->withoutMiddleware()->makePhoto();
        $this->response = $this->json('GET', '/api/photos/'.$photo->id, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);
        $this->assertApiResponse($photo->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePhoto()
    {
        $photo = $this->withoutMiddleware()->makePhoto();
        $editedPhoto = $this->fakePhotoData();

        $this->response = $this->json('PATCH', '/api/photos/'.$photo->id, $editedPhoto, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);

        $this->assertApiResponse($editedPhoto);
    }

    /**
     * @test
     */
    public function testDeletePhoto()
    {
        $photo = $this->withoutMiddleware()->makePhoto();
        $this->response = $this->json('DELETE', '/api/photos/'.$photo->id, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/photos/'.$photo->id, [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ]);

        $this->response->assertStatus(404);
    }
}
