# Studydrive API
The goal of the system is to make it available for users to register and interact with the [JSONPlaceholder Image API](http://jsonplaceholder.typicode.com/).

## Requirements
* [Git](https://git-scm.com/)
* [Docker](https://www.docker.com/)

## Installation
1 - First, clone repository:

```
$ git clone git@gitlab.com:joaopschias/studydrive-api.git
$ cd studydrive-api
```

2 - If you need to develop some code, switch to the branch develop

```
$ git checkout develop
$ git pull
```
*This step is not mandatory, you can test the project on the branch master*

3 - Create a .env file

```
$ cp .env.example .env
```

4 - Initialize Docker:

```
$ docker-compose up -d
```
*This step can take a few minutes to be done*

5 - Install/Update Composer packages:

```
$ docker-compose run app composer install
```
or
```
$ docker-compose run app composer update
```

6 - Generate Laravel project key:

```
$ docker-compose run app php artisan key:generate
```

7 -  Create tables and fill with example values:

```
$ docker-compose run app php artisan migrate --seed
```
or
```
$ docker-compose run app php artisan migrate:refresh --seed
```
## Development

Server: http://0.0.0.0:8081/

phpMyAdmin: http://0.0.0.0:8181/

## Testing
If you need to run some test in the project use:
```
$ docker-compose run app vendor/bin/phpunit
```

## End-points
If you wanna test the endpoints without the Front-end system you can use:

### POST api/login
Return with Bearer token 

**Parameters**

|          Name | Required |  Type   | Description                                                                           |
| -------------:|:--------:|:-------:| --------------------------------------------------------------------------------------|
|     `username` | required | string  | Username attribute of User                                                           |
|     `password` | required | string  | Username attribute of User                                                           | 
|     `grant_type` | required | string  | Can be password or authorization_code or refresh_token. Recommended is *password*  | 
|     `client_id` | required | int  | Client id from table oauth_clients normally the used is the number *2*                 |
|     `client_scret` | required | string  | Client value from table oauth_clients normally the used value is from the register with the id *2* |
|     `scope` | required | string  | Since we do not use access scopes, just put *                                           |

You can try with:

|          Name |  Value   | Help                                                                                    |
| -------------:|:-------:| -----------------------------------------------------------------------------------------|
|     `username` | 'neo'  | Username attribute of User                                                               |
|     `password` | 'abc123'  | Password attribute of User                                                            | 
|     `grant_type` | 'password'  |                                                                                   | 
|     `client_id` | 2  |                                                                                             |
|     `client_scret` | 'client_secret'  | Change the value 'client_secret' with the value obtained by the query SELECT * FROM `oauth_clients` WHERE `id`= 2;             |
|     `scope` | *  | Just put *                                                                                      | 

### GET api/users/favorite-photos
Return all favorites photos from the logeed user

### GET api/users/top-week-favoriters
Returns all users sorted by the number of favorite photos

### POST api/users/favorite-photo
Add a photo to favorite list of the logged user

**Parameters**

|          Name | Required |  Type   |
| -------------:|:--------:|:-------:|
|     `photo_id` | required | int|string  | 

### DELETE api/users/unfavorite-photo/{photo_id}
Remove a photo to favorite list of the logged user

**Parameters**

|          Name | Required |  Type   |
| -------------:|:--------:|:-------:|
|     `photo_id` | required | int|string  | 

### GET api/photos/recent-favorites
Returns all photos sorted by the most recent interaction with a User

**If you need additional information or another route see the file _[routes/api.php](https://gitlab.com/joaopschias/studydrive-api/blob/master/routes/api.php)_**

### Extras
Command to clear composer autoload:

```
$ docker-compose exec app composer dump-autoload
```

Command to clear application cache:

```
$ docker-compose exec app php artisan cache:clear
```

Command to clear route cache:

```
$ docker-compose exec app php artisan route:cache
```

Command to clear config cache:

```
$ docker-compose exec app php artisan config:cache
```

Command to clear compiled view files :

```
$ docker-compose exec app php artisan view:clear
```

Command to stop Docker Compose and clear all containers:

```
$ docker-compose down
$ docker rm $(docker ps -a -q); docker rmi $(docker images -q);
```

## Framework
[Laravel 5.8](https://laravel.com/docs/5.8).

### Packages
* [InfyOm - Laravel Generator](http://labs.infyom.com/laravelgenerator/)
* [CORS Middleware for Laravel 5](https://github.com/barryvdh/laravel-cors/)
* [ixudra/curl](https://github.com/ixudra/curl/)
* [Fractal](https://github.com/thephpleague/fractal/)
* [Passport](https://laravel.com/docs/5.8/passport/)

## Authors

* **João P. Schias** - [LinkedIn](https://www.linkedin.com/in/joaopschias)
