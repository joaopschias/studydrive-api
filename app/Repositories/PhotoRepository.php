<?php

namespace App\Repositories;

use App\Models\Photo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PhotoRepository
 * @package App\Repositories
 * @version July 6, 2019, 7:50 am UTC
*/

class PhotoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'url',
        'thumbnailUrl'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Photo::class;
    }

    /**
     * Get most recent favorited photos
     *
     * @param string $orderBy
     *
     * @return Photo $photos
     */
    public function recentFavorites($orderBy = 'DESC')
    {
        return $this->orderBy('last_interaction', $orderBy)->get();
    }
}
