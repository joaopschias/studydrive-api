<?php

namespace App\Repositories;

use App\Models\Photo;
use App\Models\User;
use Carbon\Carbon;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version July 6, 2019, 4:16 am UTC
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'username',
        'password',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    /**
     * Favorite Photo
     *
     * @param User $user
     * @param Photo $photo
     *
     * @return array
     */
    public function favoritePhoto(User $user, Photo $photo)
    {
        $message = 'Photo is already in Favorites List';
        if(!$user->photos()->where('photo_id', $photo->id)->first()){
            $user->photos()->save($photo);
            $photo->update([
                'last_interaction' => Carbon::now()
            ]);

            $message = 'Photo has been added to Favorites List';
        }

        return ['message' => $message, 'user' => $user];
    }

    /**
     * Unfavorite Photo
     *
     * @param User $user
     * @param Photo $photo
     *
     * @return array
     */
    public function unfavoritePhoto(User $user, Photo $photo)
    {
        $message = 'Photo is not in Favorites List';
        if ($user->photos()->where('photo_id', $photo->id)->first()) {
            $user->photos()->detach([$photo->id]);
            $message =  'Photo has been removed to Favorites List';
        }

        return ['message' => $message, 'user' => $user];
    }

    /**
     * Get the top likers
     *
     * @param string $orderBy
     *
     * @return User $users
     */
    public function topWeekFavoriters($orderBy = 'DESC')
    {
        return $this->has('week_photos')->withCount('week_photos')->orderBy('week_photos_count', $orderBy)->get();
    }
}
