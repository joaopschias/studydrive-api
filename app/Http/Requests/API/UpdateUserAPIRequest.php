<?php

namespace App\Http\Requests\API;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use InfyOm\Generator\Request\APIRequest;

class UpdateUserAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = User::$rules;

        $user_id =  Auth::user()->id;

        $rules['username'] = [
            'required',
            Rule::unique('user')->ignore($user_id)
        ];

        $rules['password'] = [
            'nullable',
            'string',
            'min:6',
            'confirmed',
        ];

        return $rules;
    }

    /**
     * Get the validation labels that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return User::$rules_labels;
    }
}
