<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use App\Repositories\PhotoRepository;
use App\Repositories\UserRepository;
use App\Transformers\PhotoTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Ixudra\Curl\Facades\Curl;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    /** @var  PhotoRepository */
    private $photoRepository;

    public function __construct(UserRepository $userRepo, PhotoRepository $photoRepo)
    {
        $this->userRepository = $userRepo;
        $this->photoRepository = $photoRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
        $paginator = $this->userRepository->paginate(20);
        $countries = $paginator->getCollection();

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());

        $resource = new Collection($countries, new UserTransformer());
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return $this->sendResponse($manager->createData($resource)->toArray(), 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     * @throws \Exception
     */
    public function store(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        $user = $this->userRepository->create($input);

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $resource = new Item($user, new UserTransformer);

        return $this->sendResponse($manager->createData($resource)->toArray(), 'User saved successfully');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());

        $resource = new Item($user, new UserTransformer);

        return $this->sendResponse($manager->createData($resource)->toArray(), 'User retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     * @throws \Exception
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user = $this->userRepository->update($input, $id);

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());

        $resource = new Item($user, new UserTransformer);

        return $this->sendResponse($manager->createData($resource)->toArray(), 'User updated successfully');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendResponse($id, 'User deleted successfully');
    }

    /**
     * Bring user data logged in
     *
     * @param Request $request
     * @return Response
     */
    public function me(Request $request)
    {
        $user = Auth::user();

        return $this->sendResponse($user, 'User retrieved successfully');
    }

    /**
     * Bring user favorite Photos data
     * GET /users/favorite-photos
     *
     * @param Request $request
     * @return Response
     */
    public function favoritePhotos(Request $request)
    {
        $user_id =  Auth::user()->id;
        $user = $this->userRepository->findWithoutFail($user_id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $photos = $user->photos()->get();

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $resource = new Collection($photos, new PhotoTransformer());

        return $this->sendResponse($manager->createData($resource)->toArray(), 'Photos retrieved successfully');
    }

    /**
     * Favorite a Photo
     * POST /users/favorite-photo
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function favoritePhoto(Request $request)
    {
        $user_id =  Auth::user()->id;
        $user = $this->userRepository->findWithoutFail($user_id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $photo = $this->photoRepository->findWithoutFail($request->get('photo_id'));

        if (empty($photo)) {
            $photoResponse = Curl::to('http://jsonplaceholder.typicode.com/photos/' . $request->get('photo_id'))->asJson()->get();
            if(!empty($photoResponse->id)){
                $photo = $this->photoRepository->create([
                    'id' => $photoResponse->id,
                    'title' => $photoResponse->title,
                    'url' => $photoResponse->url,
                    'thumbnailUrl' => $photoResponse->thumbnailUrl,
                ]);
            } else {
                return $this->sendError('Photo not found');
            }
        }

        $data = $this->userRepository->favoritePhoto($user, $photo);

        return $this->sendResponse($data['user'], $data['message']);
    }

    /**
     * Unfavorite a Photo
     * DELETE /users/favorite-photo
     *
     * @param Request $request
     *
     * @return Response
     */
    public function unfavoritePhoto($photo_id, Request $request)
    {
        $user_id =  Auth::user()->id;
        $user = $this->userRepository->findWithoutFail($user_id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $photo = $this->photoRepository->findWithoutFail($photo_id);

        if (empty($photo)) {
            return $this->sendError('Photo not found');
        }

        $data = $this->userRepository->unfavoritePhoto($user, $photo);

        return $this->sendResponse($data['user'], $data['message']);
    }

    /**
     * Bring user favorite Photos data
     * GET /users/top-likers
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function topWeekFavoriters(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
        $users = $this->userRepository->topWeekFavoriters();

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $resource = new Collection($users, new UserTransformer());

        return $this->sendResponse($manager->createData($resource)->toArray(), 'Users retrieved successfully');
    }
}
