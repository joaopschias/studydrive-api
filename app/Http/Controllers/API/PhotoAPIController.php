<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePhotoAPIRequest;
use App\Http\Requests\API\UpdatePhotoAPIRequest;
use App\Models\Photo;
use App\Repositories\PhotoRepository;
use App\Transformers\PhotoTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PhotoController
 * @package App\Http\Controllers\API
 */

class PhotoAPIController extends AppBaseController
{
    /** @var  PhotoRepository */
    private $photoRepository;

    public function __construct(PhotoRepository $photoRepo)
    {
        $this->photoRepository = $photoRepo;
    }

    /**
     * Display a listing of the Photo.
     * GET|HEAD /photos
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $this->photoRepository->pushCriteria(new RequestCriteria($request));
        $this->photoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $paginator = $this->photoRepository->paginate(20);
        $photos = $paginator->getCollection();

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());

        $resource = new Collection($photos, new PhotoTransformer());
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return $this->sendResponse($manager->createData($resource)->toArray(), 'Photos retrieved successfully');
    }

    /**
     * Store a newly created Photo in storage.
     * POST /photos
     *
     * @param CreatePhotoAPIRequest $request
     *
     * @return Response
     * @throws \Exception
     */
    public function store(CreatePhotoAPIRequest $request)
    {
        $input = $request->all();

        $photo = $this->photoRepository->create($input);

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $resource = new Item($photo, new PhotoTransformer);

        return $this->sendResponse($manager->createData($resource)->toArray(), 'Photo saved successfully');
    }

    /**
     * Display the specified Photo.
     * GET|HEAD /photos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Photo $photo */
        $photo = $this->photoRepository->find($id);

        if (empty($photo)) {
            return $this->sendError('Photo not found');
        }

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());

        $resource = new Item($photo, new PhotoTransformer());

        return $this->sendResponse($manager->createData($resource)->toArray(), 'Photo retrieved successfully');
    }

    /**
     * Update the specified Photo in storage.
     * PUT/PATCH /photos/{id}
     *
     * @param int $id
     * @param UpdatePhotoAPIRequest $request
     *
     * @return Response
     * @throws \Exception
     */
    public function update($id, UpdatePhotoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Photo $photo */
        $photo = $this->photoRepository->find($id);

        if (empty($photo)) {
            return $this->sendError('Photo not found');
        }

        $photo = $this->photoRepository->update($input, $id);

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());

        $resource = new Item($photo, new PhotoTransformer);

        return $this->sendResponse($manager->createData($resource)->toArray(), 'Photo updated successfully');
    }

    /**
     * Remove the specified Photo from storage.
     * DELETE /photos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Photo $photo */
        $photo = $this->photoRepository->find($id);

        if (empty($photo)) {
            return $this->sendError('Photo not found');
        }

        $photo->delete();

        return $this->sendResponse($id, 'Photo deleted successfully');
    }

    /**
     * Display a listing of the recently favorited Photos.
     * GET|HEAD /photos
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function recentFavorites(Request $request)
    {
        $this->photoRepository->pushCriteria(new RequestCriteria($request));
        $this->photoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $photos = $this->photoRepository->recentFavorites();

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $resource = new Collection($photos, new PhotoTransformer());

        return $this->sendResponse($manager->createData($resource)->toArray(), 'Photos retrieved successfully');
    }
}
