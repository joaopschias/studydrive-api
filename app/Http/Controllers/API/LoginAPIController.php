<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Models\User;
use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

class LoginAPIController extends AppBaseController
{
    use AuthenticatesUsers;

    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    public function username()
    {
        return 'username';
    }

    protected function register(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        $this->userRepository->create($input);

        $request->request->add([
            'username'      => $input['username'],
            'password'      => $input['password'],
        ]);

        // forward the request to the oauth token request endpoint
        $tokenRequest = Request::create(
            '/oauth/token',
            'POST'
        );

        return Route::dispatch($tokenRequest);
    }

    protected function authenticated(Request $request)
    {
        // forward the request to the oauth token request endpoint
        $tokenRequest = Request::create(
            '/oauth/token',
            'post'
        );

        return Route::dispatch($tokenRequest);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $username = $request->input('username');
        $userExists = User::where('username', $username)->first() !== null;
        $failedLoginMessageUser = Lang::get('auth.user');
        $failedLoginMessagePassword = Lang::get('auth.password');

        if (!$userExists) {
            return $this->sendError($failedLoginMessageUser, [], 401);
        }

        return $this->sendError($failedLoginMessagePassword, [], 401);
    }
}
