<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Photo
 * @package App\Models
 * @version July 6, 2019, 7:50 am UTC
 *
 * @property int id
 * @property \App\Models\User users
 * @property string title
 * @property string url
 * @property string thumbnailUrl
 * @property \Carbon\Carbon last_interaction
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 */
class Photo extends Model
{
    public $table = 'photo';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = [
        'last_interaction',
    ];

    public $fillable = [
        'id',
        'title',
        'url',
        'thumbnailUrl',
        'last_interaction',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'url' => 'string',
        'thumbnailUrl' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'max:200',
        'url' => 'required',
        'thumbnailUrl' => 'required'
    ];

    /**
     * Validation rules labels
     *
     * @var array
     */
    public static $rules_labels = [
        'title' => 'Title',
        'url' => 'URL',
        'thumbnailUrl' => 'Thumbnail URL'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'user_photo')->withTimestamps();
    }

    public function getFavorites()
    {
        return $this->users()->count();
    }
}
