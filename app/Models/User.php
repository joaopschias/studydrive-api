<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App\Models
 * @version July 6, 2019, 4:16 am UTC
 *
 * @property int id
 * @property string name
 * @property string username
 * @property string password
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    public $table = 'user';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'username',
        'password',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'username' => 'string',
        'password' => 'string',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * Validation rules labels
     *
     * @var array
     */
    public static $rules_labels = [
        'name' => 'Name',
        'username' => 'Username',
        'password' => 'Password',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'string|max:200',
        'username' => 'required|unique:user',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function photos()
    {
        return $this->belongsToMany(\App\Models\Photo::class, 'user_photo')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function week_photos()
    {
        $start = Carbon::now()->startOfWeek(Carbon::MONDAY);
        $end = Carbon::now()->endOfWeek(Carbon::SUNDAY);

        return $this->belongsToMany(\App\Models\Photo::class, 'user_photo')
                    ->wherePivot('created_at', '>=', $start)
                    ->wherePivot('created_at', '<=', $end)->withTimestamps();
    }

    /**
     * Set the user's password
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getWeekFavoritesCount()
    {
        return $this->week_photos()->count();
    }

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }
}
