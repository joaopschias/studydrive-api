<?php
namespace App\Transformers;

use App\Models\Photo;
use League\Fractal;

class PhotoTransformer extends Fractal\TransformerAbstract
{
    public function transform(Photo $photo)
    {
        return [
            'id' => $photo->id,
            'title' => $photo->title,
            'url' => $photo->url,
            'thumbnailUrl' => $photo->thumbnailUrl,
            'favorites' => $photo->getFavorites(),
            'last_interaction' => $photo->last_interaction,
            'created_at' => $photo->created_at,
            'updated_at' => $photo->updated_at,
        ];
    }
}
