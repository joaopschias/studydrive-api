<?php
namespace App\Transformers;

use App\Models\User;
use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'username' => $user->username,
            'favorites' => $user->getWeekFavoritesCount(),
            'user_photo' => $user->photos()->pluck('id'),
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
        ];
    }
}
